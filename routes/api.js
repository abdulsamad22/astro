
module.exports = function (app) {
    app.get('/api/getChannels', function (req, res, next) {
        app.controllers.channels.getAllChannels(req, res);
    });
    
    app.post('/api/updateFavourite', function (req, res, next) {
        app.controllers.channels.updateFavourite(req, res);
    });
};