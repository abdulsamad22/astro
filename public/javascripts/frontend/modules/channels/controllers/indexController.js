(function () {
    'use strict';

    angular.module('channels').controller('channels.indexController', indexController);
    indexController.$inject = ['serverSvc'];

    function indexController(serverSvc) {
        var vm = this;
        vm.channels = [];
        getChannels();

        /**
         * Mark Channel as favourite and Remove from favourite
         * @param channelId
         * @param isRemove
         */
        vm.updateFavourite = function (channelId, isRemove) {
            serverSvc.callAPI('POST', 'api/updateFavourite', {channelId: channelId, isRemove: isRemove}).then(
                function (response) {
                    vm.favouriteChannel = response;
                    angular.forEach(vm.channels, function (value) {
                        console.log(value.channelId);
                        if (!isRemove && value.channelId === channelId) {
                            value.favourite = true;
                        }

                        if (isRemove && value.channelId === channelId) {
                            delete value.favourite;
                        }
                    });
                },
                function (response) {

                }
            );

        };

        /**
         * Getting channels
         */
        function getChannels() {
            vm.error = false;
            serverSvc.callAPI('GET', 'api/getChannels').then(
                function (result) {
                    vm.itemsByPage = 15;
                    if (angular.isDefined(result) && angular.isDefined(result.channels)) {
                        vm.channels = result.channels;
                    }
                },
                function (result) {
                    vm.error = (angular.isDefined(result.data.reason.message))
                        ? result.data.reason.message
                        : 'There is some error occured.';
                }
            );
        }

    }
})();