(function () {
    'use strict';

    angular
        .module('channels')
        .config(['$stateProvider', channelsRouterConfig]);

    function channelsRouterConfig($stateProvider) {
        $stateProvider
            .state('home', {
                url: '/home',
                title: 'Astro Channel list',
                templateUrl: 'javascripts/frontend/modules/channels/views/channels.handlebars',
                controller: 'channels.indexController',
                controllerAs: 'vm'
            });

    }

})();