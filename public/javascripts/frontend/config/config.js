(function() {
  'use strict';

  angular
    .module('astro')
    .config(['$httpProvider', config]);

  function config($httpProvider) {
    // Enable log
    //$logProvider.debugEnabled(true);

    //Make sure that laravel treats our requests as ajax
    // $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    // $httpProvider.defaults.headers.common['Access-Control-Allow-Credentials'] = true;
    //So that we can send post data in delete request
    // $httpProvider.defaults.headers.delete = {'Content-Type': 'application/json'};
  }
  

})();
