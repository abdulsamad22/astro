(function () {
    'use strict';

    angular
        .module('astro')
        .constant('apiEndPoint', document.getElementsByTagName('base')[0].href)

})();