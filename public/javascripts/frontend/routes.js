(function () {
    'use strict';

    angular
        .module('astro')
        .config(['$urlRouterProvider', '$locationProvider', appRouterConfig]);

    function appRouterConfig($urlRouterProvider, $locationProvider) {
        $urlRouterProvider.otherwise('home');
        $locationProvider.html5Mode(true);

    }

})();