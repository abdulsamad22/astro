(function() {
    'use strict';

    angular
        .module('astro')
        .factory('serverSvc', ['apiEndPoint', '$filter', '$http', '$q', serverSvc]);

    function serverSvc(apiEndPoint, $filter, $http, $q) {
        var root = {

        };
        
        /**
         * $http generic wrapper
         * @param method
         * @param url
         * @param data
         * @param requestId
         * @param cache
         * @returns {*|promise}
         */
        root.callAPI = function (method, url, data, requestId, cache) {
            var deferred = $q.defer();
            var httpMethod = $filter('lowercase')(method);
            var config = {
                method: httpMethod,
                url: apiEndPoint + url,
                data: data,
                requestId: requestId,
                cache: ((angular.isDefined(cache)) ? true : false)
            };

            $http(config).then(
                function(response){
                    deferred.resolve(response.data);
                },
                function(response){
                    deferred.reject(response);
                }
            );

            return deferred.promise;
        };

        return root;
    }


})();
