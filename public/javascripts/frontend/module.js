
(function() {
    'use strict';

    angular.module('core',
        [
            'ngSanitize',
            'ui.router',
            'angular.filter',
            'smart-table'
        ]
    );


    angular.module('astro', ['core', 'channels']);

    angular.module('channels', [ 'core' ]);

    // angular.module('payment', [ 'core' ]);


})();
