Using Node.js on serverside:
Node.js version v6.9.4
NPM version 4.1.1
Express 4.15.5

Angular.js on client side:
Version 1.4.8

Packages used in project:
"angular": "^1.4.8"
"angular-filter": "^0.5.17"
"angular-sanitize": "1.4.8"
"angular-smart-table": "^2.1.8"
"angular-ui-router": "^0.4.3"
"async": "^2.5.0"
"body-parser": "~1.18.2"
"cookie": "^0.3.1"
"cookie-parser": "^1.4.3"
"debug": "~2.6.9"
"ejs": "^2.5.7"
"express": "~4.15.5"
"express-handlebars": "^3.0.0"
"express-load": "^1.1.16"
"http": "0.0.0"
"jade": "~1.11.0"
"lodash": "^4.17.4"
"morgan": "~1.9.0"
"q": "^1.5.1"
"request": "^2.83.0"
"request-promise": "^4.2.2"

Install Project dependencies: 
npm install

To start the project:
npm start

