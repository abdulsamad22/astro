var request = require('request-promise');
var q = require('q');

module.exports = {
    getChannels: function () {
        var deferred = q.defer(),
            options = {
                uri: global.API_URL + 'ams/v3/getChannels',
                method: 'GET',
                timeout: 10000,
                json: true
            };

        request(options)
            .then(function (response) {
                deferred.resolve(response);

            })
            .catch(function (err) {
                deferred.reject(err.error);
            });

        return deferred.promise;
    }
};