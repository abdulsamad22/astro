module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            app: {
                src: [
                    'public/stylesheets/style.css'
                ],
                dest: 'public/dist/assets/css/app.min.css'
            },
            vendor: {
                src: [
                    'node_modules/bootstrap/dist/css/bootstrap.min.css'
                ],
                dest: 'public/dist/assets/css/vendor.min.css'
            }
        },
        ngtemplates: {
            cgConsole1: {
                src: [
                    'public/javascripts/frontend/modules/channels/views/**.handlebars'
                ],
                dest: 'public/dist/js/templates/templates.js',
                options: {
                    module: 'astro',
                    url:    function(url) { return url.replace('public/javascripts/', 'javascripts/'); },
                    htmlmin: {
                        collapseWhitespace: true,
                        collapseBooleanAttributes: true
                    }
                }
            }
        },
        concat: {
            app: {
                src: [
                    'public/javascripts/frontend/module.js',
                    'public/javascripts/frontend/controller.js',
                    'public/javascripts/frontend/routes.js',
                    'public/javascripts/frontend/config/config.js',
                    'public/javascripts/frontend/config/constants.js',
                    'public/javascripts/frontend/services/serverSvc.js',
                    'public/javascripts/frontend/modules/channels/routes/routes.js',
                    'public/javascripts/frontend/modules/channels/controllers/indexController.js'
                ],
                dest: 'public/dist/js/app/app.js',
                options: {
                    separator: ';\n',
                    stripBanners: true,
                    process: true
                }
            },
            vendor: {
                src: [
                    'node_modules/angular/angular.min.js',
                    'node_modules/angular-sanitize/angular-sanitize.min.js',
                    'node_modules/angular-ui-router/release/angular-ui-router.min.js',
                    'node_modules/angular-filter/dist/angular-filter.min.js',
                    'node_modules/angular-smart-table/dist/smart-table.min.js'

                ],
                dest: 'public/dist/js/vendor/vendor.js',
                options: {
                    separator: ';\n',
                    stripBanners: true,
                    process: true
                }
            }
        },
        uglify: {
            app: {
                src: 'public/dist/js/app/app.js',
                dest: 'public/dist/js/app/app.min.js',
                options: {
                    compress: true,
                    mangle: false,
                    mangleProperties: false,
                    sourceMap: false
                }
            },
            vendor: {
                src: 'public/dist/js/vendor/vendor.js',
                dest: 'public/dist/js/vendor/vendor.min.js',
                options: {
                    compress: true,
                    mangle: true,
                    sourceMap: false
                }
            }
        },
        processhtml: {
            build1: {
                files: {
                    'views/layouts/main_live.handlebars': ['views/layouts/main.handlebars']
                }
            }
        },
        clean: {
            excludeVendor: {
                options: {
                    force: true
                },
                src: [
                    'public/dist/assets/**',
                    'public/dist/js/templates/**',
                    'public/dist/js/app/**'
                ]
            },
            dist: {
                options: {
                    force: true
                },
                src: [ 'public/dist/**' ]
            }
        }

    });

    // Load the plugin that provides the "less" task.
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-angular-templates');
    grunt.loadNpmTasks('grunt-minjson');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-processhtml');

    // Default task(s).
    grunt.registerTask('excludeVendor', ['clean:excludeVendor', 'cssmin', 'ngtemplates', 'concat', 'uglify:app', 'processhtml', 'copy', 'versioning']);
    grunt.registerTask('dist',          ['clean:dist', 'cssmin', 'ngtemplates', 'concat', 'uglify', 'processhtml']);

};
