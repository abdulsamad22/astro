/**
 * Getting all channels from Astro API
 * Binding Favourites which fetched channels against new property: favourite
 * Using cookies to fetch data of favourite channels
 * @param req
 * @param res
 */
exports.getAllChannels = function (req, res) {
    req.app.services.channels.getChannels().then(
        function (response) {
            var favourites = (global.lodash.isUndefined(req.cookies.astroAsignFav)) ? "" : req.cookies.astroAsignFav;
            if (favourites !== "") {
                global.lodash.forEach(response.channel, function (value, key) {
                    if (global.lodash.indexOf(favourites, value.channelId) !== -1) {
                        value.favourite = true;

                    } else {
                        value.favourite = false;
                    }
                });
            }

            res.json({status: true, channels: response.channel});

        }, function (reason) {
            var status = global.lodash.isUndefined(reason.status)? 400 : reason.status;
            res.status(status).json({status: false, reason: reason});

        });
};


/**
 * Marking channels as favourite and remove from favourites
 * Using cookies to save favourite channels with 2 days of lifetime
 * @param req
 * @param res
 */
exports.updateFavourite = function (req, res) {
    var options = {
        maxAge: 1000 * 60 * 60 * 48,
        httpOnly: true
    };

    var favourites = (global.lodash.isUndefined(req.cookies.astroAsignFav)) ? [] : req.cookies.astroAsignFav;
    if (req.body.isRemove) {
        favourites = global.lodash.filter(favourites, function (channelId) {
            return channelId !== req.body.channelId;
        });

    } else {
        favourites.push(req.body.channelId);
    }

    res.cookie('astroAsignFav', favourites, options);
    res.json({status: true, response: favourites});
};