# Astro Assignment


# Using Node.js on serverside:
    Node.js version v6.9.4
    NPM version 4.1.1
    Express 4.15.5

# Angular.js on client side:
    Version 1.4.8

# Packages used in project:
    "angular": "^1.4.8",
    "angular-filter": "^0.5.17",
    "angular-sanitize": "1.4.8",
    "angular-smart-table": "^2.1.8",
    "angular-ui-router": "^0.4.3",
    "async": "^2.5.0",
    "body-parser": "~1.18.2",
    "cookie": "^0.3.1",
    "cookie-parser": "^1.4.3",
    "debug": "~2.6.9",
    "express": "~4.15.5",
    "express-handlebars": "^3.0.0",
    "express-load": "^1.1.16",
    "grunt": "^0.4.5",
    "grunt-angular-templates": "^0.5.9",
    "grunt-contrib-clean": "^0.7.0",
    "grunt-contrib-concat": "^0.5.1",
    "grunt-contrib-cssmin": "^0.14.0",
    "grunt-contrib-uglify": "^0.11.0",
    "grunt-minjson": "^0.4.0",
    "grunt-processhtml": "^0.3.8",
    "http": "0.0.0",
    "load-grunt-tasks": "^3.5.2",
    "lodash": "^4.17.4",
    "morgan": "~1.9.0",
    "q": "^1.5.1",
    "request": "^2.83.0",
    "request-promise": "^4.2.2",
    "serve-favicon": "~2.4.5"


# Install Project dependencies: 
    sudo apt-get update
    wget http://nodejs.org/dist/v0.6.9/node-v0.6.9.tar.gz
    tar -xvzf node-v0.6.9.tar.gz
    cd node-v0.6.9
    ./configure
    make
    sudo make install
    which node
    npm install -g grunt-cli
    npm install

# To start the project:
    npm start

